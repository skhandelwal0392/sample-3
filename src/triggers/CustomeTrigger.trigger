trigger CustomeTrigger on Case (before insert,before update) {
    
    if(Trigger.isBefore)
    {
    if(Trigger.isInsert){
    for(Case cs:Trigger.new){
        if(cs.Status=='New'){
            cs.Percentage_Completion__c=25;
        }
        else if(cs.Status=='Working'){
            cs.Percentage_Completion__c=50;
        }
        else if(cs.Status=='Esclated'){
            cs.Percentage_Completion__c=75;
        }
        else{
            cs.Percentage_Completion__c=100;
        }
    }
    }
        
        
    
    if(Trigger.isUpdate){
        
        Map<id,Case> oldCaseData=Trigger.oldMap;
        for(Case cs:Trigger.new){
            if(oldCaseData.get(cs.id).Status=='Esclated')
                cs.addError('Error in updation');
        }
    }
}
   
}