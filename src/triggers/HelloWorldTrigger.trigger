trigger HelloWorldTrigger on Book__c (before insert) {
    
    Book__c[] sBook=Trigger.new;
    
    MyHelloWorldClass.applyDiscountOnPrices(sBook);

}